package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import com.econetwireless.utils.messages.AirtimeBalanceResponse;
import com.econetwireless.utils.messages.AirtimeTopupRequest;
import com.econetwireless.utils.pojo.INBalanceResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EnquiriesServiceImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditsServiceImplTest.class);


    @Test
    public void testEnquireMethod() {
        String partnerCode = "hot-recharge";
        String msisdn ="263774222193";
        SubscriberRequestDao subscriberRequestDao = mock(SubscriberRequestDao.class);
        ChargingPlatform chargingPlatform = mock(ChargingPlatform.class);
        EnquiriesServiceImpl enquiriesService = new EnquiriesServiceImpl(chargingPlatform, subscriberRequestDao);


        final AirtimeTopupRequest airtimeTopupRequest = new AirtimeTopupRequest();
        airtimeTopupRequest.setPartnerCode(partnerCode);
        airtimeTopupRequest.setReferenceNumber("TOPUP-REF-0123");
        airtimeTopupRequest.setAmount(1.11);
        airtimeTopupRequest.setMsisdn("772984803");


        SubscriberRequest subscriberRequest = new SubscriberRequest();
        subscriberRequest.setId((long) 2);
        subscriberRequest.setRequestType("Airtime Topup");
        subscriberRequest.setPartnerCode(partnerCode);
        subscriberRequest.setMsisdn("263774222193");
        subscriberRequest.setBalanceBefore(0.39);
        subscriberRequest.setBalanceAfter(2.41);
        subscriberRequest.setAmount(0.00);
        subscriberRequest.setDateCreated(new Date());
        subscriberRequest.setDateLastUpdated(new Date());
        subscriberRequest.setStatus("Successful");
        subscriberRequest.setReference("TOPUP-REF-01");
        subscriberRequest.setVersion((long) 2.02);

        INCreditRequest inCreditRequest = new INCreditRequest();
        inCreditRequest.setAmount(200);
        inCreditRequest.setMsisdn("263774222193");
        inCreditRequest.setPartnerCode(partnerCode);
        inCreditRequest.setReferenceNumber("TOPUP-REF-01");

        INCreditResponse inCreditResponse = new INCreditResponse();
        inCreditResponse.setBalance(300);
        inCreditResponse.setMsisdn("263774222193");
        inCreditResponse.setNarrative("Credit Success");
        inCreditResponse.setResponseCode("200");

        INBalanceResponse inBalanceResponse = new INBalanceResponse();
        inBalanceResponse.setAmount(300);
        inBalanceResponse.setMsisdn("263774222193");
        inBalanceResponse.setNarrative("Credit Success");
        inBalanceResponse.setResponseCode("200");

        List<SubscriberRequest> subscriberRequestList = new ArrayList<>();
        subscriberRequestList.add(subscriberRequest);
        when(subscriberRequestDao.save(any(SubscriberRequest.class))).thenReturn(subscriberRequest);
        when(subscriberRequestDao.save(any(SubscriberRequest.class))).thenReturn(subscriberRequest);
        when(chargingPlatform.enquireBalance(any(String.class), any(String.class))).thenReturn(inBalanceResponse);
        AirtimeBalanceResponse airtimeBalanceResponse = enquiriesService.enquire(partnerCode, msisdn);
        LOGGER.info("creditsService.credit(airtimeTopupRequest): {}", airtimeBalanceResponse);
        assertEquals("200", airtimeBalanceResponse.getResponseCode());
    }
}