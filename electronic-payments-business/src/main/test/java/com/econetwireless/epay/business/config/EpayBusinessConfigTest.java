package com.econetwireless.epay.business.config;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.business.services.api.CreditsService;
import com.econetwireless.epay.business.services.api.EnquiriesService;
import com.econetwireless.epay.business.services.api.PartnerCodeValidator;
import com.econetwireless.epay.business.services.api.ReportingService;
import com.econetwireless.epay.dao.requestpartner.api.RequestPartnerDao;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotNull;

public class EpayBusinessConfigTest {

    @Mock
    private ChargingPlatform chargingPlatform;

    @Mock
    private SubscriberRequestDao subscriberRequestDao;

    @Mock
    private RequestPartnerDao requestPartnerDao;

    @InjectMocks
    private EpayBusinessConfig epayBusinessConfig;

    @Before
    public void initialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreditsService() {
        CreditsService creditsService = epayBusinessConfig.creditsService(chargingPlatform, subscriberRequestDao);
        assertNotNull(creditsService);
    }

    @Test
    public void testEnquiriesService() {
        EnquiriesService enquiriesService = epayBusinessConfig.enquiriesService(chargingPlatform, subscriberRequestDao);
        assertNotNull(enquiriesService);
    }

    @Test
    public void testReportingService() {
        ReportingService reportingService = epayBusinessConfig.reportingService(subscriberRequestDao);
        assertNotNull(reportingService);
    }

    @Test
    public void testPartnerCodeValidator() {
        PartnerCodeValidator partnerCodeValidator = epayBusinessConfig.partnerCodeValidator(requestPartnerDao);
        assertNotNull(partnerCodeValidator);
    }
}