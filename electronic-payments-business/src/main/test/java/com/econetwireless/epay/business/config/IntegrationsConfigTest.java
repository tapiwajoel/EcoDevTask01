package com.econetwireless.epay.business.config;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.in.soap.service.IntelligentNetworkService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class IntegrationsConfigTest {

    @Test
    public void testChargingPlatform() {
        IntegrationsConfig integrationsConfig = new IntegrationsConfig();
        IntelligentNetworkService intelligentNetworkService = mock(IntelligentNetworkService.class);
        ChargingPlatform chargingPlatform = integrationsConfig.chargingPlatform(intelligentNetworkService);
        assertNotNull(chargingPlatform);
    }

    @Test
    public void intelligentNetworkService() {
        IntegrationsConfig integrationsConfig = new IntegrationsConfig();
        JaxWsPortProxyFactoryBean jaxWsPortProxyFactoryBean = integrationsConfig.intelligentNetworkService();
        assertNotNull(jaxWsPortProxyFactoryBean);
    }
}