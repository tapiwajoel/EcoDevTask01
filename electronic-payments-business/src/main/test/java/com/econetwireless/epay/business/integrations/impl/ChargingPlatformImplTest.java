package com.econetwireless.epay.business.integrations.impl;

import com.econetwireless.epay.business.services.impl.CreditsServiceImpl;
import com.econetwireless.in.soap.service.BalanceResponse;
import com.econetwireless.in.soap.service.CreditRequest;
import com.econetwireless.in.soap.service.CreditResponse;
import com.econetwireless.in.soap.service.IntelligentNetworkService;
import com.econetwireless.utils.pojo.INBalanceResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChargingPlatformImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditsServiceImpl.class);

    private IntelligentNetworkService intelligentNetworkService;
    private ChargingPlatformImpl chargingPlatformImpl;
    private String partnerCode;

    @Before
    public void initMocks() {
        partnerCode = "hot-recharge";
        intelligentNetworkService = mock(IntelligentNetworkService.class);
        chargingPlatformImpl = new ChargingPlatformImpl(intelligentNetworkService);
    }

    @Test
    public void testCreditSubscriberAccount() {

        INCreditRequest inCreditRequest = new INCreditRequest();
        inCreditRequest.setAmount(200);
        inCreditRequest.setMsisdn("263774222193");
        inCreditRequest.setPartnerCode(partnerCode);
        inCreditRequest.setReferenceNumber("TOPUP-REF-01");


        INCreditResponse inCreditResponse = new INCreditResponse();
        inCreditResponse.setBalance(300);
        inCreditResponse.setMsisdn("263774222193");
        inCreditResponse.setNarrative("Credit Success");
        inCreditResponse.setResponseCode("200");

        CreditResponse creditResponse = new CreditResponse();
        creditResponse.setBalance(300);
        creditResponse.setMsisdn("263774222193");
        creditResponse.setNarrative("Credit Success");
        creditResponse.setResponseCode("200");

        when(intelligentNetworkService.creditSubscriberAccount(any(CreditRequest.class))).thenReturn(creditResponse);
        INCreditResponse inCreditResponse1 = chargingPlatformImpl.creditSubscriberAccount(inCreditRequest);

        assertEquals("200", inCreditResponse1.getResponseCode());
    }

    @Test
    public void testEnquireBalance() {
        BalanceResponse balanceResponse = new BalanceResponse();
        balanceResponse.setAmount(300);
        balanceResponse.setMsisdn("263774222193");
        balanceResponse.setNarrative("Credit Success");
        balanceResponse.setResponseCode("200");

        when(intelligentNetworkService.enquireBalance(any(String.class), any(String.class))).thenReturn(balanceResponse);

        String msisdn = "263774222193";
        INBalanceResponse inBalanceResponse = chargingPlatformImpl.enquireBalance(partnerCode, msisdn);
        assertEquals("200", inBalanceResponse.getResponseCode());
    }
}